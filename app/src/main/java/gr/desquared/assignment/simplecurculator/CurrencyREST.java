package gr.desquared.assignment.simplecurculator;

/**
 * SimpleCurculator
 * Created by Jc on 9/12/2017.
 */

import gr.desquared.assignment.simplecurculator.models.ExchangeRates;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CurrencyREST {

    /**
     * Retrieves a specific rate
     *
     * @param base    (i.e USD)
     * @param symbols (i.e USD,GBP)
     * @return JSON Object (i.e { "base":"EUR","date":"2017-09-11","rates":{ "GBP":0.90775 } } )
     */
    @GET("/latest")
    Call<ExchangeRates> getRates(@Query("base") String base, @Query("symbols") String symbols);

    /**
     * Retrieves a list of all currencies available in the API
     *
     * @return JSON Object (i.e { "base":"EUR","date":"2017-09-11","rates":{ "GBP":0.90775,"BGN": 1.639,"BRL": 3.1105,"CAD": 1.2132,"CHF": 0.95902,"CNY": 6.5385,"CZK": 21.876,... } } )
     */
    @GET("/latest")
    Call<ExchangeRates> getCurrencies();
}