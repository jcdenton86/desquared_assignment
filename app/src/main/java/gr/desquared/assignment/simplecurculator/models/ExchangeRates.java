package gr.desquared.assignment.simplecurculator.models;

/**
 * SimpleCurculator
 * Created by Jc on 9/12/2017.
 */

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class ExchangeRates {
    @SerializedName("base")
    public String base;
    @SerializedName("date")
    public String date;
    @SerializedName("rates")
    public Map<String,Float> rates;
}

