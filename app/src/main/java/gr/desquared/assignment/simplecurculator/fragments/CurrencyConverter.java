package gr.desquared.assignment.simplecurculator.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import gr.desquared.assignment.simplecurculator.CurrencyRESTCalls;
import gr.desquared.assignment.simplecurculator.R;
import gr.desquared.assignment.simplecurculator.listener.ExchangeRatesListener;
import gr.desquared.assignment.simplecurculator.utils.ConnectivityUtils;
import gr.desquared.assignment.simplecurculator.utils.CurculatorUtils;

public class CurrencyConverter extends DialogFragment implements ExchangeRatesListener {

    private final String TAG = this.getClass().getName();
    Spinner spinner1;
    Spinner spinner2;
    EditText editCur;
    TextView cresult;
    TextView latestrate;
    Button btnConvert;
    ArrayAdapter<String> adapter;

    public CurrencyConverter() {
    }

    public static CurrencyConverter newInstance() {
        return new CurrencyConverter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_item, new ArrayList<>());
        if(ConnectivityUtils.checkConnection(getActivity())) {
            CurrencyRESTCalls.request(this);
        } else {
            // use offline available currencies to populate the spinners (not for particular use
            // just to show that here we would handle an offline event and retrieve rates from local
            // resources (i.e. a db etc.) or we could just disable the currency conversion when
            // offline
            adapter.addAll(CurculatorUtils.currencySymbols.keySet());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.currency_dialog_title));
        return inflater.inflate(R.layout.fragment_currency_converter, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editCur = view.findViewById(R.id.curFrom);
        latestrate = view.findViewById(R.id.latestrate);
        spinner1 = view.findViewById(R.id.spinner1);
        spinner2 = view.findViewById(R.id.spinner2);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner2.setAdapter(adapter);

        cresult = view.findViewById(R.id.cresult);

        btnConvert = view.findViewById(R.id.convert);
        btnConvert.setOnClickListener(btnConvertClick);
    }

    private View.OnClickListener btnConvertClick = (v) -> {
        String curFrom = spinner1.getSelectedItem().toString();
        String curTo = spinner2.getSelectedItem().toString();
        if(ConnectivityUtils.checkConnection(getActivity())) {
            if (editCur.getText().toString().trim().length() > 0) {
                double amount = Double.parseDouble(editCur.getText().toString().replace(',', '.'));
                if (!Double.isNaN(amount)) {
                    CurrencyRESTCalls.request(this, curFrom, curTo, amount);
                }
            }
        } else {
            //snackbar/toast a message 'no internet connection'
        }
    };

    @Override
    public void onExhangeRatesResponse(String curFrom, String curTo, double rate, double result) {
        //Log.i(TAG, "Exchange rate: " + result);
        cresult.setText(String.format(Locale.getDefault(), "%.5f", result));
        curFrom = CurculatorUtils.currencySymbols.getOrDefault(curFrom, curFrom);
        curTo = CurculatorUtils.currencySymbols.getOrDefault(curTo, curTo);
        latestrate.setText(1 + " " + curFrom + ": " + String.format(Locale.getDefault(), "%.5f", rate) + " " + curTo);
    }

    @Override
    public void onCurrenciesReponse(ArrayList<String> currencies) {
        Log.i(TAG, currencies.size() + "");
        adapter.addAll(currencies);
    }
}
