package gr.desquared.assignment.simplecurculator;

import android.util.Log;

import java.util.ArrayList;
import java.util.Map;

import gr.desquared.assignment.simplecurculator.listener.ExchangeRatesListener;
import gr.desquared.assignment.simplecurculator.models.ExchangeRates;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * SimpleCurculator
 * Created by Jc on 9/12/2017.
 */

public class CurrencyRESTCalls {

    private static final String BASE_URL = "http://api.fixer.io";
    private static Retrofit retrofit = null;

    private static Retrofit getClientInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static void request(ExchangeRatesListener l, final String cur1, String cur2, double amount) {
        CurrencyREST apiService =
                getClientInstance().create(CurrencyREST.class);

        Call<ExchangeRates> call = apiService.getRates(cur1, cur1 + "," + cur2);
        call.enqueue(new Callback<ExchangeRates>() {
            @Override
            public void onResponse(Call<ExchangeRates> call, Response<ExchangeRates> response) {
                if (!response.body().rates.isEmpty()) {
                    l.onExhangeRatesResponse(cur1, cur2, response.body().rates.get(cur2), (response.body().rates.get(cur2) * amount));
                }
            }

            @Override
            public void onFailure(Call<ExchangeRates> call, Throwable t) {
                Log.e("Retrofit Error", t.toString());
            }
        });
    }

    public static void request(ExchangeRatesListener l) {
        CurrencyREST apiService =
                getClientInstance().create(CurrencyREST.class);

        Call<ExchangeRates> call = apiService.getCurrencies();
        call.enqueue(new Callback<ExchangeRates>() {
            @Override
            public void onResponse(Call<ExchangeRates> call, Response<ExchangeRates> response) {
                Map<String, Float> rates = response.body().rates;
                ArrayList<String> currencies = new ArrayList<>(rates.keySet());
                currencies.add(0,response.body().base);
                l.onCurrenciesReponse(currencies);
            }

            @Override
            public void onFailure(Call<ExchangeRates> call, Throwable t) {
                Log.e("Retrofit Error", t.toString());
            }
        });
    }
}

