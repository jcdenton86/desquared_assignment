package gr.desquared.assignment.simplecurculator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import gr.desquared.assignment.simplecurculator.fragments.CalculatorFragment;

public class MainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Class fragmentClass;

        fragmentClass = CalculatorFragment.class;

        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {Log.i(TAG,e.getLocalizedMessage());}

        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.popBackStack();
        fragmentManager.beginTransaction()
                .replace(R.id.flContent, fragment, fragmentClass.getName())
                .commit();

    }
}
