package gr.desquared.assignment.simplecurculator.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * SimpleCurculator
 * Created by Jc on 9/12/2017.
 */

public abstract class CurculatorUtils {

    public final static String ONE = "1";
    public final static String TWO = "2";
    public final static String THREE = "3";
    public final static String FOUR = "4";
    public final static String FIVE = "5";
    public final static String SIX = "6";
    public final static String SEVEN = "7";
    public final static String EIGHT = "8";
    public final static String NINE = "9";
    public final static String ZERO = "0";

    /* Available Operators */
    public final static String DOT = ".";
    public final static String ADD = "+";
    public final static String SUBTRACT = "-";
    public final static String MULTIPLY = "*";
    public final static String DIVIDE = "/";

    /* Currencies to symbol mapping (the API does not return symbols) */
    public final static Map<String,String> currencySymbols;
    static
    {
        currencySymbols = new HashMap<>();
        currencySymbols.put("EUR", "€");
        currencySymbols.put("AUD", "$");
        currencySymbols.put("BGN", "лв");
        currencySymbols.put("BRL", "R$");
        currencySymbols.put("CAD", "C$");
        currencySymbols.put("CHF", "CHF");
        currencySymbols.put("CNY", "¥");
        currencySymbols.put("CZK", "Kč");
        currencySymbols.put("DKK", "kr");
        currencySymbols.put("GBP", "£");
        currencySymbols.put("HKD", "HK$");
        currencySymbols.put("HRK", "kn");
        currencySymbols.put("HUF", "Ft");
        currencySymbols.put("IDR", "Rp");
        currencySymbols.put("ILS", "₪");
        currencySymbols.put("INR", "Rs.");
        currencySymbols.put("JPY", "¥");
        currencySymbols.put("KRW", "₩");
        currencySymbols.put("MXN", "$");
        currencySymbols.put("MYR", "RM");
        currencySymbols.put("NOK", "kr");
        currencySymbols.put("NZD", "$");
        currencySymbols.put("PHP", "₱");
        currencySymbols.put("PLN", "zł");
        currencySymbols.put("RON", "L");
        currencySymbols.put("RUB", "руб");
        currencySymbols.put("SEK", "kr");
        currencySymbols.put("THB", "฿");
        currencySymbols.put("TRY", "YTL");
        currencySymbols.put("USD", "$");
        currencySymbols.put("ZAR", "R");
    }
}

