package gr.desquared.assignment.simplecurculator.listener;

import java.util.ArrayList;

/**
 * SimpleCurculator
 * Created by Jc on 9/12/2017.
 */

public interface ExchangeRatesListener {
    void onExhangeRatesResponse(String curFrom, String curTo, double singlerate, double result);

    void onCurrenciesReponse(ArrayList<String> currencies);
}
