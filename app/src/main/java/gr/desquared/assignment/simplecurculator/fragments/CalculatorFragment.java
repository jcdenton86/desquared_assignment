package gr.desquared.assignment.simplecurculator.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DecimalFormat;

import gr.desquared.assignment.simplecurculator.R;
import gr.desquared.assignment.simplecurculator.databinding.FragmentCalculatorBinding;
import gr.desquared.assignment.simplecurculator.utils.CurculatorUtils;


public class CalculatorFragment extends Fragment {

    private FragmentCalculatorBinding binding;

    private final String TAG = this.getClass().getName();

    private String OPERATOR;

    private double valueOne = Double.NaN;
    private double valueTwo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_calculator, container, false);
        initUI(binding);

        return binding.getRoot();
    }

    private void computeCalculation() {
        if (!Double.isNaN(valueOne) && binding.input.getText().toString().length() > 0) {
            valueTwo = Double.parseDouble(binding.input.getText().toString());
            binding.input.setText(null);

            switch (OPERATOR) {
                case CurculatorUtils.ADD:
                    valueOne = this.valueOne + valueTwo;
                    break;
                case CurculatorUtils.SUBTRACT:
                    valueOne = this.valueOne - valueTwo;
                    break;
                case CurculatorUtils.MULTIPLY:
                    valueOne = this.valueOne * valueTwo;
                    break;
                case CurculatorUtils.DIVIDE:
                    if (valueTwo == 0f) {
                        Log.i(TAG, valueTwo + "");
                        this.valueOne = Double.NaN;
                        valueTwo = Double.NaN;
                        break;
                    }
                    valueOne = this.valueOne / valueTwo;
                    break;
            }
        } else {
            try {
                valueOne = Double.parseDouble(binding.input.getText().toString());
            } catch (Exception e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
        }
    }

    private void initUI(final FragmentCalculatorBinding binding) {

        final DecimalFormat decimalFormat = new DecimalFormat("#.#####");

        binding.dot.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.DOT));
        binding.zero.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.ZERO));
        binding.one.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.ONE));
        binding.two.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.TWO));
        binding.three.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.THREE));
        binding.four.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.FOUR));
        binding.five.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.FIVE));
        binding.six.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.SIX));
        binding.seven.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.SEVEN));
        binding.eight.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.EIGHT));
        binding.nine.setOnClickListener(view -> binding.input.setText(binding.input.getText() + CurculatorUtils.NINE));

        binding.add.setOnClickListener(view -> {
            computeCalculation();
            OPERATOR = CurculatorUtils.ADD;
            binding.result.setText(decimalFormat.format(valueOne) + CurculatorUtils.ADD);
            binding.input.setText(null);
        });

        binding.subtract.setOnClickListener(view -> {
            computeCalculation();
            OPERATOR = CurculatorUtils.SUBTRACT;
            binding.result.setText(decimalFormat.format(valueOne) + CurculatorUtils.SUBTRACT);
            binding.input.setText(null);
        });

        binding.multiply.setOnClickListener(view -> {
            computeCalculation();
            OPERATOR = CurculatorUtils.MULTIPLY;
            binding.result.setText(decimalFormat.format(valueOne) + CurculatorUtils.MULTIPLY);
            binding.input.setText(null);
        });

        binding.divide.setOnClickListener(view -> {
            computeCalculation();
            OPERATOR = CurculatorUtils.DIVIDE;
            binding.result.setText(decimalFormat.format(valueOne) + CurculatorUtils.DIVIDE);
            binding.input.setText(null);
        });

        binding.equal.setOnClickListener(view -> {
            computeCalculation();
            binding.result.setText(binding.result.getText().toString() +
                    decimalFormat.format(valueTwo) + " = " + decimalFormat.format(valueOne));
            valueOne = Double.NaN;
            OPERATOR = "0";
        });

        binding.clear.setOnClickListener(view -> {
            if (binding.input.getText().length() > 0) {
                CharSequence currentText = binding.input.getText();
                binding.input.setText(currentText.subSequence(0, currentText.length() - 1));
            } else {
                valueOne = Double.NaN;
                valueTwo = Double.NaN;
                binding.input.setText("");
                binding.result.setText("");
            }
        });

        binding.convert.setOnClickListener((v) -> {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("CurrencyConverter");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            CurrencyConverter newFragment = CurrencyConverter.newInstance();
            newFragment.show(ft, "CurrencyConverter");
        });
    }
}